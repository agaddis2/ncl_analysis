;========================================================================================
; Filename: annmonthlyavg.ncl
; Description:
; Calculates an average for each month from an NCEP data set and subtracts these 
; monthly averages from another time period in the same data set. Useful for 
; determining variance from the mean and EOFs. Also optionally prints out the monthly 
; averages as netcdf files.
;
; Date: 6/18/2012
; Created by Abigail Gaddis, University of Tennessee
;=======================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

; User defined parameters
;----------------------------------------------------------------------------------------

   avYrStrt = 1981                               ;start year for average
   avMonStrt = 01                        ;select 01=January - 12=December
   avYrLast = 1990                               ;final year for average
   avMonLast = 12                        ;select 01=January - 12=December

   yrStrt  = 1991                        ;start year for data of interest
   monStrt = 01                          ;select 01=January - 12=December
   yrLast  = 1993                        ;final year for data of interest
   monLast = 01                          ;select 01=January - 12=December
   inputfile = "hgt.mon.mean"
   outputfile = "diff.hgt"

; Read variable over desired time range from file
;----------------------------------------------------------------------------------------

   filein      = addfile (inputfile+".nc", "r")
   TIME   = filein->time                              ;read in file's time variable
   yyyy   = ut_calendar(TIME,-1)                 ;convert to YYYYMM calendar
   lat    = filein->lat
   lon    = filein->lon

;computing and selecting time range for the average
   avStrt = avYrStrt*100 + avMonStrt
   avLast = avYrLast*100 + avMonLast
   avyrange = ind(yyyy.ge.avStrt .and. yyyy.le.avLast)  ;time range for avg
   avnmon = dimsizes(avyrange)                   ;number of months in avg range
   varAv  = filein->hgt(avyrange,6,:,:)               ;read in variable over avyrange
   avtime = TIME(avyrange)

;computing and selecting time range for anomaly
   strt = yrStrt*100 + monStrt
   last = yrLast*100 + monLast
   yrange = ind(yyyy.ge.strt .and. yyyy.le.last)  ;year range for data
   nmon   = dimsizes(yrange)                     ;number of months in data range
   var    = filein ->hgt(yrange,6,:,:)
   time   = TIME(yrange)
   var!0  = "time"
  
   rad    = 4.*atan(1.)/180.
  
   print(avnmon)                         ;check number of avg months
   print(nmon)                           ;check number of data months

; Climatological monthly mean of the data 
;----------------------------------------------------------------------------------------

   varAv = new((/12,dimsizes(lat), dimsizes(lon)/), typeof(varAv))
   varAv = clmMonTLL(var) 

; Subtract the monthly average from each month in data of interest
;----------------------------------------------------------------------------------------
  varAnom = new (dimsizes(var), typeof(var))
  i=0
  do year=yrStrt,yrLast
    if (year.eq.yrStrt) then
      ms = monStrt     
    else
      ms = 01 
    end if
    if (year.eq.yrLast) then
      me = monLast
    else
      me = 12
    end if
    do month=ms,me
      yearmon = year*100 + month
      varAnom(:,:,i) = var(:,:,i)-varAv(month-1,:,:)
      i=i+1
    end do
  end do

copy_VarMeta(var,varAnom)

; Output differences and averages as netcdf files, plot differences
;----------------------------------------------------------------------------------------

  system ("rm -f " + outputfile + ".nc")        ;remove pre-existing file
  ncdf   = addfile(outputfile+".nc" ,"c")       ;open output netCDF file
  ncdf->hgt = varAnom  

;  ncdf   = addfile("av."+outputfile+".nc" ,"c")
;  filedimdef(ncdf,"time",-1,True) 
;  ncdf->hgt = av  

wks = gsn_open_wks("ps",outputfile)
gsn_define_colormap(wks,"BlGrYeOrReVi200")

res=True
res@cnFillOn=True
res@cnLinesOn=False
res@cnLineLabelsOn=False
res@gsnSpreadColors=True
res@cnInfoLabelOn=True
res@cnInfoLabelPerimOn=False
res@cnInfoLabelSide="Top"
res@cnInfoLabelFontHeightF=0.012
res@pmLabelBarOrthogonalPosF=0.02
res@pmLabelBarHeightF=0.20

dims = dimsizes(varAnom)
monthnames = (/"January","February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"/)
monthnumber = monStrt-1
yearnumber = yrStrt

do i=0,dims(2)-1
  res@cnInfoLabelString= "Difference of " + sprinti("%i",avYrStrt)+"-"+sprinti("%i",avYrLast)+" average "+monthnames(monthnumber)+" and "+ monthnames(monthnumber)+ " " + sprinti("%i",yearnumber)
  d = gsn_csm_contour_map(wks,varAnom(:,:,i),res)
  monthnumber = (monthnumber+1)%12
  if (monthnumber.eq.0) then
    yearnumber = yearnumber+1
  end if
end do

delete(wks)
