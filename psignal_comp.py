#!/opt/local/bin/python2.7
# #!/usr/bin/env

import sys
import os
from optparse import OptionParser
import subprocess
import collections
import numpy as np
import netCDF4
import glob
import pylab as pl
import matplotlib.pyplot as plt
import itertools


#######################################################################
# User specified settings
#######################################################################

# File to be read containing signal to noise ratios and stds
filename = 'signalSTD.nc'
# Eliminate variables with signals below this cutoff
signalcutoff = 3.0
# Name of dataset (appended to some variable names in file)
forced_str = 'FAMIP'

#######################################################################
# Read in .nc file
#######################################################################

f = netCDF4.Dataset(filename, 'r')

time = f.variables["time"]
#netCDF4.num2date returns datetime objects given numeric time values
time = netCDF4.num2date(time, time.units, time.calendar)
#print dir(time)

lev = f.variables["lev"]

variables = f.variables
vNames = variables.keys()
nNames = len(vNames)

filedims = f.dimensions
dsizes = [len(value) for key,value in filedims.iteritems()]

signalstr = "_signal"
forcedstd = "FAMIP"
unforcstd = "noPin"

#######################################################################
# Defining variables
#######################################################################

signal2Dlist = list()
signal1Dlist = list()
Smax2Dlist = list()
Smax1Dlist = list()
var2Dname = list()
var1Dname = list()
std_noPin1Dlist = list()
std_noPin2Dlist = list()
std_1Dlist = list()
std_2Dlist = list()

#######################################################################
# Sort variables by name into lists, exclude if Smax < signalcutoff
#######################################################################

for v in range(nNames) :
    varname = vNames[v]
    varname.encode('utf-8')
    variable = variables[varname]
    vardims = variable.dimensions
    vardimnum = len(vardims)
    # time,level variables
    if vardimnum == 2 :
        if signalstr in varname :
            Smax2D = [max(variable[:,l]) for l in range(len(lev))]
            signal2Dlist.append(variable)
            Smax2Dlist.append(Smax2D)
            # removing "_signal" from variable name and adding to list
            varname = varname.replace(signalstr,"")
            var2Dname.append(varname)
        # unforced standard deviations
        elif unforcstd in varname : 
            std_noPin2Dlist.append(variable)
        # forced standard deviations
        elif forcedstd in varname : 
            std_2Dlist.append(variable)
    # time variables
    elif vardimnum == 1 : 
        if signalstr in varname :
            Smax1D = max(variable)
            if Smax1D > signalcutoff : 
                signal1Dlist.append(variable)
                Smax1Dlist.append(Smax1D)
                varname = varname.replace(signalstr,"")
                var1Dname.append(varname)
            else:
                print varname + " skipped"
                del vardims
                del variable
                break
        # unforced standard deviations
        elif unforcstd in varname :
            std_noPin1Dlist.append(variable)
        # forced standard deviations
        elif forcedstd in varname :
            std_1Dlist.append(variable)
    del vardims
    del variable
n2D = len(signal2Dlist)
n1D = len(signal1Dlist)

#######################################################################
# Count longest duration above cutoff
#######################################################################

dur2Dlist = list() 
dur1Dlist = list()

# Counting the max duration above signalcutoff of all 1D variables
for signal1D in signal1Dlist : 
    count = 0
    dur1D = 0
    for st in signal1D :
        if (st == signal1D._FillValue) :
            count = 0
            continue
        elif (st <= signalcutoff) :
            count = 0
        elif (st > signalcutoff) :
            count = count + 1
            if (count > dur1D):
                dur1D = count
    dur1Dlist.append(dur1D)

# Counting the max duration above signalcutoff of all 2D variables for each level
for signal2D in signal2Dlist :
    dur2Dlevlist = list()
    for l in range(dsizes[1]):
        count = 0
        dur2D = 0
        for t in range(dsizes[0]):        
            if signal2D[t,l] == signal2D._FillValue:
                count = 0
                continue
            elif signal2D[t,l] <= signalcutoff :
                count = 0
            elif signal2D[t,l] > signalcutoff :
                count = count + 1
                if (count > dur2D) :
                    dur2D = count
        dur2Dlevlist.append(dur2D)
    dur2Dlist.append(dur2Dlevlist)
    del dur2Dlevlist 

#######################################################################
# Send trimmed output to ncl for plotting
#######################################################################

#temp = f.variables['T_signal']
#for signal1D in signal1Dlist :
#plt.rc('lines', linewidth=2)
#fig, (ax0, ax1)  = plt.subplots(nrows=2)
#plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])

#print time

#plt.figure()
markers = itertools.cycle([ '+', '*', ',', 'o', '.', '1', 'p' ])
for signal1D in signal1Dlist:
    plt.plot(x=time, y=signal1D, marker=next(markers))
#    plt.draw()
plt.title('Title')
#plt.legend( l, varnames, 'upper right', shadow=False)
plt.xlabel('time')
plt.ylabel('y')
plt.show()
#plt.savefig('fig.png')

#######################################################################
# Output data as .nc for plotting and .txt file for LaTeX table
#######################################################################





