;=============================================================================
; Calculates a long term monthly global average for temperature and subtracts 
; it from another time period in the same dataset. The result is plotted 
; versus aersol optical depth.
;=============================================================================

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------
; User defined parameters
;----------------------------------------------------

  avYrStrt = 1981				;start year for average
  avMonStrt = 01			;select 01=January - 12=December
  avYrLast = 1990				;final year for average
  avMonLast = 12			;select 01=January - 12=December 

  yrStrt  = 1991			;start year for data of interest
  monStrt = 01				;select 01=January - 12=December
  yrLast  = 1994 			;final year for data of interest
  monLast = 01				;select 01=January - 12=December

  inputfile1 = "air.mon.mean"
  inputfile2 = "output"
  inputfile3 = "t85f09.FAMIPa.noPin"
  inputfile4 = "ammann2003b_volcanics"

  ; Names for plot and file labeling
  outputfile 	= "TempPhaseT85"
  dataset1	= "NCEP Reanalysis"
  dataset2	= "ERA-40 Reanalysis"
  dataset3	= "CAM4 CESM"
  dataset4	= "Volcanic aerosol optical depth"

;----------------------------------------------------
; Read variable over desired time range from file
;----------------------------------------------------

  f      = addfile (inputfile1+".nc", "r")
  f2     = addfile (inputfile2+".nc", "r")
  h	 = addfile (inputfile3+".nc", "r")
  f4	 = addfile (inputfile4+".nc", "r")

  TIME   = f->time                              ;read in file's time variable
  TIME2  = int2dble(f2->time)
  TIME3  = h->time
  TIME4  = int2dble(f4->time)
  yyyy   = ut_calendar(TIME,-1)                 ;convert to YYYYMM calendar
  yyyy2  = ut_calendar(TIME2,-1)
  yyyy3	 = ut_calendar(TIME3,-1)
  lat    = f->lat
  lat2	 = h->LAT
  lat3	 = f4->lat
  lon    = f->lon

;computing and selecting time range for the average

  avStrt = avYrStrt*100 + avMonStrt
  avLast = avYrLast*100 + avMonLast
 
  avyrange = ind(yyyy.ge.avStrt .and. yyyy.le.avLast)  ;time range for avg
  avyrange2 = ind(yyyy2.ge.avStrt .and. yyyy2.le.avLast)
  avyrange3 = ind(yyyy3.ge.avStrt .and. yyyy3.le.avLast)
  avnmon = dimsizes(avyrange)			;number of months in avg range
 
  avvar1  = f->air(avyrange,:,:)    	  	;read in variable over avyrange
  avvar2  = short2flt(f2->t2m(avyrange2,:,:))
  avvar3  = h->TS(avyrange3,:,:)
 
  avtime1 = TIME(avyrange)
  avtime2 = TIME2(avyrange2)
  avtime3 = TIME3(avyrange3)

; computing and selecting time range for data of interest

  strt   = yrStrt*100 + monStrt
  last   = yrLast*100 + monLast
  yrange = ind(yyyy.ge.strt .and. yyyy.le.last)  ;year range for data
  yrange2 = ind(yyyy2.ge.strt .and. yyyy2.le.last)
  yrange3 = ind(yyyy3.ge.strt .and. yyyy3.le.last)
  yrange4 = ind(TIME4.ge.strt .and. TIME4.le.last)
  nmon   = dimsizes(yrange)			;number of months in data range
  var1	 = f->air(yrange,:,:)
  var2	 = short2flt(f2->t2m(yrange2,:,:))
  var3	 = h->TS(yrange3,:,:)
  var4	 = f4->TAUSTR(yrange4,:)
  time   = TIME(yrange)
  time2  = TIME2(yrange2)
  time3  = h->time(yrange3)
  time4	 = TIME4(yrange4)

  printVarSummary(time)
  printVarSummary(time2)
  printVarSummary(time3)

;----------------------------------------------------
; Calculate the weights
;----------------------------------------------------

  wgt  = NormCosWgtGlobe(lat)
  wgt2 = NormCosWgtGlobe(lat2)
  wgt3 = NormCosWgtGlobe(lat3)

;----------------------------------------------------
; Convert other data to NCEP units
;----------------------------------------------------
 
  time2   = time2+(1900-1)*365.2422*24 
  time2@units = "hours since 1-1-1 00:00:0.0"
  time3   = time3*24+365.2422*24*(1975-1)+243*24 
  time3@units = "hours since 1-1-1 00:00:0.0"

  avtime2   = avtime2+1899*365.2422*24 
  avtime2@units = "hours since 1-1-1 00:00:0.0"
  avtime3   = avtime3*24+365.2422*24*(1975-1)+243*24 
  avtime3@units = "hours since 1-1-1 00:00:0.0"

  var2 = var2-273.15
  var2@units = "degC"
  var3 = var3-273.15
  var3@units = "degC"

  avvar2 = avvar2-273.15
  avvar2@units = "degC"
  avvar3 = avvar3-273.15
  avvar3@units = "degC"

  printVarSummary(time)
  printVarSummary(time2)
  printVarSummary(time3)


;----------------------------------------------------
; Reorder data
;----------------------------------------------------

  ravvar1 = avvar1(time|:,lat|:,lon|:)
  ravvar2 = avvar2(time|:,latitude|:,longitude|:)
  ravvar3 = avvar3(time|:,LAT|:,LON|:)
  rvar1 = var1(time|:,lat|:,lon|:)
  rvar2 = var2(time|:,latitude|:,longitude|:)
  rvar3 = var3(time|:,LAT|:,LON|:)

  copy_VarAtts(var1,rvar1)
  copy_VarAtts(var2,rvar2)
  copy_VarAtts(var3,rvar3)
  copy_VarAtts(avvar1,ravvar1)
  copy_VarAtts(avvar2,ravvar2)
  copy_VarAtts(avvar3,ravvar3)

;printVarSummary(rvar3)
;printVarSummary(ravvar3)

;----------------------------------------------------
; Calculate global weighted averages
;----------------------------------------------------
 
  globav1 = wgt_areaave_Wrap(ravvar1,wgt,1.0,0)
  globav2 = wgt_areaave_Wrap(ravvar2,wgt,1.0,0)
  globav3 = wgt_areaave_Wrap(ravvar3,wgt2,1.0,0)

  globvar1 = wgt_areaave_Wrap(rvar1,wgt,1.0,0)
  globvar2 = wgt_areaave_Wrap(rvar2,wgt,1.0,0)
  globvar3 = wgt_areaave_Wrap(rvar3,wgt2,1.0,0)
  globvar4 = dble2flt(dim_avg_wgt_Wrap(var4,wgt3,1))

;-------------------------------------------------------------------
; Take time average over avyrange for each month, retaining metadata
;-------------------------------------------------------------------

  av1 = new(12, typeof(globav1))
  av2 = new(12, typeof(globav2))
  av3 = new(12, typeof(globav3))

  do i=0,11
     av1(i) = dim_avg_Wrap(globav1(i:avnmon-1:12))
     av2(i) = dim_avg_Wrap(globav2(i:avnmon-1:12))
     av3(i) = dim_avg_Wrap(globav3(i:avnmon-1:12))
  end do

;-------------------------------------------------------------------
; Subtract the monthly average from each month in data of interest
;-------------------------------------------------------------------

  vardiff1 = new(dimsizes(globvar1), typeof(globvar1))
  vardiff2 = new(dimsizes(globvar2), typeof(globvar2))
  vardiff3 = new(dimsizes(globvar3), typeof(globvar3))
;  vardiff4 = new(dimsizes(globvar3), typeof(globvar3))

  i=0
  do year=yrStrt,yrLast
    if (year.eq.yrStrt) then
      ms = monStrt     
    else
      ms = 01 
    end if
    if (year.eq.yrLast) then
      me = monLast
    else
      me = 12
    end if
    do month=ms,me
      yearmon = year*100 + month
      vardiff1(i) = globvar1(i)-av1(month-1)
      vardiff2(i) = globvar2(i)-av2(month-1)
      vardiff3(i) = globvar3(i)-av3(month-1)
;      vardiff4(i) = globvar3(i)-av2(month-1)  
      i=i+1
    end do
  end do

  copy_VarMeta(globvar1,vardiff1)
  copy_VarMeta(globvar2,vardiff2)
  copy_VarMeta(globvar3,vardiff3)
;  copy_VarMeta(globvar4,vardiff4)

printVarSummary(vardiff1)
printVarSummary(vardiff2)
printVarSummary(vardiff3)

;------------------------------------------------------
; Make a vector including both time series of averages 
;------------------------------------------------------

  vardiffs 		= new((/3,dimsizes(vardiff1&time)/),float)
  vardiffs(0,:) 	= vardiff1
  vardiffs(1,:) 	= vardiff2
  vardiffs(2,:)		= vardiff3
;  vardiffs(3,:)		= vardiff4

; printVarSummary(vardiffs)

;----------------------------------------------------
; Create plot
;----------------------------------------------------
 
  ;create plot environment

  wks 		= gsn_open_wks("pdf",outputfile)        ; creat a plot file

  ;variables for labeling

  labels 	= (/dataset1,dataset2,dataset3/)
  colors	= (/"blue","green","black"/)

  years 	= (time)/(24*365.2422)+1
  dates 	= (/"1. January 1991","2. June 1991","3. November 1991","4. January 1993"/)
  datesnum	= (/"1","2","3","4"/)
  datevals	= (/0,5,10,24/)

  ;set up plots

   res				= True
   res@gsnDraw     		= False
   res@gsnFrame    		= False

   res@tiMainString 		= "Global Temperature Anomaly Phase Map"
   res@tiXAxisString		= dataset4
   res@tiYAxisString		= "Surface Temperature Anomaly ("+vardiff1@units+")"

   res@gsnScale  		= True        ; force text scaling 
   res@gsnMaximize		= True 
   res@tiXAxisFontHeightF     	= 0.020
   res@tiYAxisFontHeightF     	= 0.020
   res@xyLineColors           	= colors
   res@xyLineThicknesses       	= (/1.0, 1.5, 2.0, 2.5/)
   res@pmLegendDisplayMode    	= "Always"
   res@pmLegendSide           	= "Bottom"
   res@pmLegendWidthF         	= 0.14         
   res@pmLegendHeightF        	= 0.12
   res@xyExplicitLegendLabels 	= labels
   res@lgLabelFontHeightF     	= 0.012         
   res@gsnYRefLine            	= 0.0
   res@tmLabelAutoStride      	= True

   res@trXMinF			= -0.01
   res@trXMaxF			= 0.201
   res@trYMinF			= -0.60
   res@trYMaxF			= 0.60
  
   plot 		= gsn_csm_xy(wks,globvar4,vardiffs,res)

   txres			= True
   txPerimOn			= True
   txres@txFontHeightF 		= 0.010        
   txres@txJust			= "TopCenter"
   mres                		= True         ; marker resources
   mres@gsMarkerIndex  		= 16           ; marker style (filled circle)
   mres@gsMarkerSizeF  		= 7.0          ; marker size

   do c=0, dimsizes(colors)-1
      mres@gsMarkerColor  = colors(c)      ; marker colors
      txres@txFontColor  	= colors(c)
      gsn_polymarker(wks,plot,globvar4(datevals),vardiffs(c,datevals),mres)
      text = gsn_add_text(wks,plot,datesnum,globvar4(datevals)+0.003,vardiffs(c,datevals),txres)
   end do
   txres@txJust		= "CenterLeft"
   txres@txFontColor  	= "black"
   txid = gsn_add_text(wks,plot,dates,(/0.001,0.001,0.001,0.001/),(/-0.4,-0.45,-0.5,-0.55/),txres)

   draw(plot)
   frame(wks)

   res2				= res
   res2@tiMainString 		= "Difference from "+avYrStrt+"-"+avYrLast+" Average"
   res2@tiYAxisString		= "Surface Temperature Anomaly ("+vardiff1@units+")"
   res2@tiXAxisString		= "Time (years)"

   res2@tmXBMode 		= "Explicit"	
   res2@tmXBValues 		= (/1991.01,1992.01,1993.01,1994.01/)
   res2@tmXBLabels 		= (/"1991","1992","1993","1994"/)
   res2@tmXBMinorValues		= years

   plot2 = gsn_csm_xy(wks,years,vardiffs,res3); create plot
