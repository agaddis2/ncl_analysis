;========================================================================================
; Filename: varprint.ncl
; Description: Reads in a climate model file and prints out the variable names
; Date: 8/17/2012
; Created by Abigail Gaddis, University of Tennessee
;========================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

; Read in list of files

   ;read in FAMIP ensemble directory for climatology
   diravg = "/tmp/work/aag/repos/ncl_analysis/FAMIP/"
   filavg = systemfunc("cd "+diravg+"  ; ls t85*cam2.h0.nc")
;   filavg = systemfunc("cd "+diravg+"  ; ls t85*clm2.h0.nc")
;   filavg = systemfunc("cd "+diravg+"  ; ls decAnom*clm2.h0.nc")
   ;print(filavg)                  ; all file names
   avgfilelist = addfiles(diravg+filavg, "r")
   vNames = getfilevarnames (avgfilelist[0]) ; get names of all variables in one file
   nNames = dimsizes (vNames)   ; number of variables on the file

; Read in variables in the files and print out the long name and number of the variable

   print("Printing variables in file with variable 0 = 1 : ")
   do n=0,nNames-1   ;read all variables in the file, variable 38 is the first 3D variable
      varname = vNames(n)
      var = avgfilelist[0]->$varname$
      print ((n) + ", " +varname+ ", " +var@long_name)
      delete(var)
   end do
