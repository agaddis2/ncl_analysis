;========================================================================================
; Filename: tempvsobs.ncl
;
; Description: Computes the ensemble global averages and statistics of two ensembles of 
; model data. Plots the averages and standard deviations shaded around the averages with 
; time. Does this for temperature and plots results with observed temperature data. 
;
; Date: 11/03/2012
; Created by Abigail Gaddis, University of Tennessee
;=======================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

   avYrStrt = 1981                               ;start year for average
   avMonStrt = 01                        ;select 01=January - 12=December
   avYrLast = 1990                               ;final year for average
   avMonLast = 12                        ;select 01=January - 12=December

   yrStrt  = 1991                        ;start year for data of interest
   monStrt = 01                          ;select 01=January - 12=December
   yrLast  = 1994                        ;final year for data of interest
   monLast = 12                          ;select 01=January - 12=December

   ;File labeling and names
   obsfname1 = "air.mon.mean"
   obsfname2 = "T.monthly.ERA40.pressure"

   dataset1     = "CESM1.0 FAMIP ensemble"
   dataset2     = "CESM1.0 FAMIP no Pinatubo ensemble"
   obsname1 = "NCEP/NCAR"
   obsname2 = "ERA-40"
   
   varname = "T"

;----------------------------------------------------------------------------------------
; Read in list of files
;----------------------------------------------------------------------------------------

   ;read in FAMIP ensemble decadal anomalies
   dir = "/tmp/work/aag/repos/ncl_analysis/FAMIP/"
   fil = systemfunc("cd "+dir+"  ; ls decAnom*")
   filelist = addfiles(dir+fil, "r")
   vNames = getfilevarnames (filelist[0]) ; get names of all variables in file
   nNames = dimsizes (vNames)   ; number of variables on the file

   ;read in FAMIP minus Pinatubo ensemble decadal anomalies
   noPindir = "/tmp/work/aag/repos/ncl_analysis/noPin/"
   noPinfil = systemfunc("cd "+noPindir+"  ; ls decAnom*")
   noPinfilelist = addfiles(noPindir+noPinfil, "r")

   ;read in observation raw data
   obsfname1 = "air.mon.mean"
   obsfname2 = "T.monthly.ERA40.pressure"
   obsname1 = "NCEP/NCAR"
   obsname2 = "ERA-40"

   ;----------------------------------------------------------------------------------------
   ; Read in variables from files
   ;----------------------------------------------------------------------------------------

   ; Read in dimensions from file, compute dimension sizes
   time = filelist[0]->time
   lat = filelist[0]->lat
   lon = filelist[0]->lon
   lev = filelist[0]->lev
   nmon = dimsizes(time)
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nlevs = dimsizes(lev)
   nFAMIP = dimsizes(fil)

   ;reading in observation datasets   
   obsfile1 = addfile (obsfname1+".nc", "r")
   obsfile2 = addfile (obsfname2+".nc", "r")
   obstime1 = obsfile1->time
   obstime2 = int2dble(obsfile2->time)
   olat = obsfile1->lat
   olon = obsfile1->lon

   npts = nmon

   ; Initializing variables
   vars_FAMIP4D = new ((/nFAMIP, nmon, nlevs, nlats, nlons/), float)
   vars_FAMIP4D!1 = "time"
   vars_FAMIP4D!2 = "lev"
   vars_FAMIP4D!3 = "lat"
   vars_FAMIP4D!4 = "lon"
   vars_FAMIP4D&time = time
   vars_FAMIP4D&lev = lev
   vars_noPin4D = vars_FAMIP4D
   globavs_FAMIP4D = vars_FAMIP4D(:,:,:,0,0)
   globavs_noPin4D = vars_noPin4D(:,:,:,0,0)
   ensembleav_FAMIP4D = globavs_FAMIP4D(0,:,:)
   ensembleav_noPin4D = globavs_noPin4D(0,:,:)
   std_FAMIP4D = globavs_FAMIP4D(0,:,:)
   std_noPin4D = globavs_noPin4D(0,:,:)

   ;----------------------------------------------------------------------------------------
   ; Get variable for select time range in observations
   ;----------------------------------------------------------------------------------------

   yyyy1   = cd_calendar(obstime1,-1)                 ;convert to YYYYMM calendar
   yyyy2  = cd_calendar(obstime2,-1)
   strt   = yrStrt*100 + monStrt
   last   = yrLast*100 + monLast
   avStrt = avYrStrt*100 + avMonStrt
   avLast = avYrLast*100 + avMonLast  
   yrange1 = ind(yyyy1.ge.strt .and. yyyy1.le.last)  ;year range for data
   yrange2 = ind(yyyy2.ge.strt .and. yyyy2.le.last)
   avyrange1 = ind(yyyy1.ge.avStrt .and. yyyy1.le.avLast)
   avyrange2 = ind(yyyy2.ge.avStrt .and. yyyy2.le.avLast)
   obsvar1 = short2flt(obsfile1->air(yrange1,:,:,:))
   obsvar2 = short2flt(obsfile2->t(yrange2,:,:,:))
   avnmon = dimsizes(avyrange1)                   ;number of months in avg range
   avvar1  = short2flt(obsfile1->air(avyrange1,:,:,:))                ;read in variable over avyrange
   avvar2  = short2flt(obsfile2->t(avyrange2,:,:,:))
   otime = obstime1(yrange1)

   ;----------------------------------------------------
   ; Formatting observation data correctly 
   ;----------------------------------------------------

   obsvar1 = obsvar1+273.15
   avvar1 = avvar1+273.15
   avvar1@units = "K"
   obsvar1@units = "K"
 
   ;----------------------------------------------------------------------------------------
   ; Read in model variable from files
   ;----------------------------------------------------------------------------------------

   print ("Calculating ensemble statistics and comparing with observations for variable " + varname)

   outputfile = varname+"vsobs"

   ; Read in decadal anomalies of model variable
   do i = 0,dimsizes(fil)-1
      vars_FAMIP4D(i,:,:,:,:) = filelist[i]->$varname$
      vars_noPin4D(i,:,:,:,:) = noPinfilelist[i]->$varname$
   end do

   ;-------------------------------------------------------------------
   ; Observation monthly climatology, retaining metadata
   ;-------------------------------------------------------------------

   av1 = clmMonTLLL(avvar1)
   av2 = clmMonTLLL(avvar2)

   ;-------------------------------------------------------------------
   ; Observation monthly anomaly
   ;-------------------------------------------------------------------

   obsanom1 = obsvar1
   obsanom2 = obsvar2
   i=0
   do year=yrStrt,yrLast
      if (year.eq.yrStrt) then
         ms = monStrt
      else
         ms = 01
      end if
      if (year.eq.yrLast) then
         me = monLast
      else
         me = 12
      end if
      do month=ms,me
         yearmon = year*100 + month
         obsanom1(i,:,:,:) = obsvar1(i,:,:,:)-av1(month-1,:,:,:)
         obsanom2(i,:,:,:) = obsvar2(i,:,:,:)-av2(month-1,:,:,:)
         i=i+1
      end do
   end do

   copy_VarMeta(obsvar1,obsanom1)
   copy_VarMeta(obsvar2,obsanom2)

   ;----------------------------------------------------------------------------------------
   ; Calculate global weighted averages
   ;----------------------------------------------------------------------------------------

   wgt = NormCosWgtGlobe(lat)
   owgt = NormCosWgtGlobe(olat)
   
   do i = 0,dimsizes(fil)-1
      globavs_FAMIP4D(i,:,:) = wgt_areaave_Wrap(vars_FAMIP4D(i,:,:,:,:), wgt, 1.0, 0)
      globavs_noPin4D(i,:,:) = wgt_areaave_Wrap(vars_noPin4D(i,:,:,:,:), wgt, 1.0, 0)
   end do

   obswgt1 = wgt_areaave_Wrap(obsanom1, owgt, 1.0, 0)
   obswgt2 = wgt_areaave_Wrap(obsanom2, owgt, 1.0, 0)   

   ;----------------------------------------------------------------------------------------
   ; Calculate comparison statistics between ensembles
   ;----------------------------------------------------------------------------------------

   ensembleav_FAMIP4D = (globavs_FAMIP4D(0,:,:)+globavs_FAMIP4D(1,:,:)+globavs_FAMIP4D(2,:,:)+ \
                         globavs_FAMIP4D(3,:,:)+globavs_FAMIP4D(4,:,:)+globavs_FAMIP4D(5,:,:))/6
   ensembleav_noPin4D = (globavs_noPin4D(0,:,:)+globavs_noPin4D(1,:,:)+globavs_noPin4D(2,:,:)+ \
                         globavs_noPin4D(3,:,:)+globavs_noPin4D(4,:,:)+globavs_noPin4D(5,:,:))/6
   std_FAMIP4D = dim_stddev_n(globavs_FAMIP4D,0)
   std_noPin4D = dim_stddev_n(globavs_noPin4D,0)

   if (all (std_noPin4D .eq. 0)) then   ;if the standard deviation is all 0, skip
      print(varname+ " has standard deviation value of zero. Skipping variable.")
      ; deleting variables that change size if 3D vs 4D
      delete(vardims)
      continue
   end if

   ;----------------------------------------------------------------------------------------
   ; Create plot
   ;----------------------------------------------------------------------------------------

   ;create plot environment
   wks          = gsn_open_wks("pdf",outputfile)
   res          = True

   ;set up variables for plot labels
   labels = (/dataset1,dataset2,obsname1,obsname2/)

   ; Time series of ensemble averages
   prgb = namedcolor2rgb("LightPink")
   brgb = namedcolor2rgb("LightBlue")
   idum = NhlNewColor(wks,prgb(0,0),prgb(0,1),prgb(0,2))
   idum = NhlNewColor(wks,brgb(0,0),brgb(0,1),brgb(0,2))
   res@gsnDraw            = False             ; don't draw yet
   res@gsnFrame           = False             ; don't advance frame yet

   res@tiXAxisString            = "Time (years)"
   res@tiXAxisFontHeightF       = 0.020
   res@tiYAxisFontHeightF       = 0.020
   res@xyLineThicknesses        = (/2.0,2.0,2.0,2.0/)
   res@xyLineColors             = (/"red","blue"/)
   res@gsnScale                 = True        ; force text scaling
   res@gsnMaximize              = True
   res@pmLegendDisplayMode      = "Always"
   res@pmLegendSide             = "Bottom"
   res@pmLegendWidthF           = 0.14          ; legend width
   res@pmLegendHeightF          = 0.12
   res@xyExplicitLegendLabels   = labels
   res@lgLabelFontHeightF       = 0.014         ; font height
   res@gsnYRefLine              = 0.0
   res@tmLabelAutoStride        = True
   res@tmXBMode                 = "Explicit"
   res@tmXBValues             = (/time(0),time(11),time(23),time(35),time(47)/)
   res@tmXBLabels             = (/"1991","1992","1993","1994","1995"/)
   res@tmXBMinorValues          = time

   topline_FAMIP = ensembleav_FAMIP4D
   botline_FAMIP = ensembleav_FAMIP4D
   topline_noPin = ensembleav_noPin4D
   botline_noPin = ensembleav_noPin4D

   topline_FAMIP = ensembleav_FAMIP4D + std_FAMIP4D
   botline_FAMIP = ensembleav_FAMIP4D - std_FAMIP4D
   topline_noPin = ensembleav_noPin4D + std_noPin4D
   botline_noPin = ensembleav_noPin4D - std_noPin4D

   res@tiYAxisString            = vars_FAMIP4D@long_name+" ("+vars_FAMIP4D@units+")"
   level = vars_FAMIP4D&lev
   ; Define a polygon centered with width of 2 sigma for each ensemble
   xp1 = new((/2*nmon/), typeof(time))
   yp1 = new((/nlevs, 2*nmon/), float)
   yp2 = yp1
   do t=0,nmon-1
     yp1(:,t)          = topline_FAMIP(t,:)
     yp1(:,2*nmon-1-t) = botline_FAMIP(t,:)
     xp1(t)          = time(t)
     xp1(2*nmon-1-t) = time(t)
     yp2(:,t)          = topline_noPin(t,:)
     yp2(:,2*nmon-1-t) = botline_noPin(t,:)
   end do
   gsres                   = True
   gsres@tfPolyDrawOrder   = "Predraw"
   res@xyLineColors             = (/"red","blue"/)
   samelevs = (/10.,50.,70.,100.,200.,300.,400.,500.,600.,700.,850./)
   do l = 0,dimsizes(samelevs)-1
      val1 = samelevs(l)
      val2 = floattoint(samelevs(l)) 
      ; Calculating the maximum value for Y axis
      if (max(topline_FAMIP(:,{val1})) .gt. max(topline_noPin(:,{val1}))) then
         if (max(obswgt1(:,{val1})) .gt. (max(obswgt2(:,{val2}))))
            if (max(obswgt1(:,{val1})) .gt. max(topline_FAMIP(:,{val1})))
               maxplot = max(obswgt1(:,{val1})) 
            else
               maxplot = max(topline_FAMIP(:,{val1}))
            end if
         else            ; max FAMIP > max noPin, obs2 > obs1
            if (max(obswgt2(:,{val2})) .gt. max(topline_FAMIP(:,{val1})))
               maxplot = max(obswgt2(:,{val2}))
            else
               maxplot = max(topline_FAMIP(:,{val1}))
            end if
         end if
       else              ;max noPin > max FAMIP
         if (max(obswgt1(:,{val1})) .gt. (max(obswgt2(:,{val2}))))
            if (max(obswgt1(:,{val1})) .gt. max(topline_noPin(:,{val1})))
               maxplot = max(obswgt1(:,{val1}))
            else
               maxplot = max(topline_noPin(:,{val1}))
            end if
         else            ; max noPin > max FAMIP, obs2 > obs1
            if (max(obswgt2(:,{val2})) .gt. max(topline_noPin(:,{val1})))
               maxplot = max(obswgt2(:,{val2}))
            else
               maxplot = max(topline_noPin(:,{val1}))
            end if
         end if
      end if
      ; Calculating the minimum value for Y axis
      if (min(botline_FAMIP(:,{val1})) .lt. min(botline_noPin(:,{val1}))) then
         if (min(obswgt1(:,{val1})) .lt. (min(obswgt2(:,{val2}))))
            if (min(obswgt1(:,{val1})) .lt. min(botline_FAMIP(:,{val1})))
               minplot = min(obswgt1(:,{val1}))
            else
               minplot = min(botline_FAMIP(:,{val1}))
            end if
         else            ; min FAMIP < min noPin, obs2 < obs1
            if (min(obswgt2(:,{val2})) .lt. min(botline_FAMIP(:,{val1})))
               minplot = min(obswgt2(:,{val2}))
            else
               minplot = min(botline_FAMIP(:,{val1}))
            end if
         end if
       else              ;min noPin < min FAMIP
         if (min(obswgt1(:,{val1})) .lt. (min(obswgt2(:,{val2}))))
            if (min(obswgt1(:,{val1})) .lt. min(botline_noPin(:,{val1})))
               minplot = min(obswgt1(:,{val1}))
            else
               minplot = min(botline_noPin(:,{val1}))
            end if 
         else            ; min noPin < min FAMIP, obs2 < obs1
            if (min(obswgt2(:,{val2})) .lt. min(botline_noPin(:,{val1})))
               minplot = min(obswgt2(:,{val2}))
            else
               minplot = min(botline_noPin(:,{val1}))
            end if
         end if
      end if
      res@trYMaxF                  = maxplot
      res@trYMinF                  = minplot
      res@gsnLeftString = "Vertical Pressure Level = " + sprintf("%1.0f",samelevs(l))
      plot = gsn_csm_xy(wks,time,(/ensembleav_FAMIP4D(:,{val1}),ensembleav_noPin4D(:,{val1}), obswgt1(:,{(val1)}), obswgt2(:,{val2})/),res) 
      gsres@gsFillColor       = "LightPink"
      poly1 = gsn_add_polygon (wks,plot,xp1,yp1({val1},:),gsres)
      gsres@gsFillColor       = "LightBlue"
      poly2 = gsn_add_polygon (wks,plot,xp1,yp2({val1},:),gsres)
      draw(plot)
      frame(wks)
   end do

