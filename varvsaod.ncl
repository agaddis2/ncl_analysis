;=============================================================================
; Calculates a global weighted average for a netcdf file and draws a time
; series plot for the desired time period.
;=============================================================================

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------
; User defined parameters
;----------------------------------------------------

  yrStrt  = 1961                        ;start year for data of interest
  monStrt = 01                          ;select 01=January - 12=December
  yrLast  = 1994                        ;final year for data of interest
  monLast = 12                          ;select 01=January - 12=December

  inputfile1 = "air.mon.mean"
  inputfile2 = "output"
  inputfile3 = "ammann2003b_volcanics"

  ; Names for plot and file labeling
  outputfile    = "TempvsAOD"
  dataset1      = "NCEP observed temperature"
  dataset2      = "ERA-40 observed temperature"
  dataset3      = "Volcanic aerosol optical depth"

;----------------------------------------------------
; Read variable over desired time range from file
;----------------------------------------------------

  f      = addfile (inputfile1+".nc", "r")
  g      = addfile (inputfile2+".nc", "r")
  h      = addfile (inputfile3+".nc", "r")
  TIME   = f->time                              ;read in file's time variable
  TIME2  = int2dble(g->time)
  TIME3  = int2dble(h->time)
  yyyy   = ut_calendar(TIME,-1)                 ;convert to YYYYMM calendar
  yyyy2  = ut_calendar(TIME2,-1)
  lat    = f->lat
  lat2   = h->lat
  lon    = f->lon

; computing and selecting time range for data of interest
  strt   = yrStrt*100 + monStrt
  last   = yrLast*100 + monLast
  yrange = ind(yyyy.ge.strt .and. yyyy.le.last)  ;year range for data
  yrange2 = ind(yyyy2.ge.strt .and. yyyy2.le.last)
  yrange3 = ind(TIME3.ge.strt .and. TIME3.le.last)
  nmon   = dimsizes(yrange)                     ;number of months in data range
  var1   = f->air(yrange,:,:)
  var2   = short2flt(g->t2m(yrange2,:,:))
  var3   = h->TAUSTR(yrange3,:)
  time   = TIME(yrange)
  time2  = TIME2(yrange2)
 time3  = TIME3(yrange3)

;printVarSummary(var1)
;printVarSummary(var2)
printVarSummary(var3)
;printVarSummary(time2)
printVarSummary(time3)

;----------------------------------------------------
; Calculate the weights
;----------------------------------------------------

  wgt  = NormCosWgtGlobe(lat)
  wgt2 = NormCosWgtGlobe(lat2)

;----------------------------------------------------
; Convert other data to NCEP units
;----------------------------------------------------

  time2   = time2+1899*362.2422*24
  time2@units = "hours since 1-1-1 00:00:0.0"
  var2 = var2-273.15
  var2@units = "degC"

;printVarSummary(var2)
;printVarSummary(time2)

;----------------------------------------------------
; Reorder data
;----------------------------------------------------

  rvar1 = var1(time|:,lat|:,lon|:)
  rvar2 = var2(time|:,latitude|:,longitude|:)

  copy_VarAtts(var1,rvar1)
  copy_VarAtts(var2,rvar2)

;----------------------------------------------------
; Calculate global weighted averages
;----------------------------------------------------

  globav1 = wgt_areaave_Wrap(rvar1,wgt,1.0,0)
  globav2 = wgt_areaave_Wrap(rvar2,wgt,1.0,0)
  globav3 = dble2flt(dim_avg_wgt_Wrap(var3,wgt2,1))

;printVarSummary(globav2)
;printVarSummary(globav3)

;----------------------------------------------------
; Create plot
;----------------------------------------------------

  ;create plot environment

  wks           = gsn_open_wks("ps",outputfile)        ; creat a plot file
  res           = True
  ;set up plots

  res=True
  res@tiMainString              = "Global Average Temperature versus Forcing"
  res@tiXAxisFontHeightF        = 0.020
  res@tiYAxisFontHeightF        = 0.020
  res@gsnScale                  = True        ; force text scaling
  res@gsnMaximize               = True
  res@tmLabelAutoStride         = True ; nice tick mark labels
  res@tiXAxisString             = dataset3
  res@tiYAxisString             = dataset1+" (blue)"
  res@xyLineColors              = "blue"
  res@xyLineThicknesses         = 2

  res2                          = res
  res2@tiYAxisString            = dataset2+" (green)"
  res2@xyLineColors             = "green"
  res2@xyLineThicknesses        = 1

  plot = gsn_csm_xy2(wks,globav3,globav1,globav2,res,res2)
