;========================================================================================
; Filename: signal.ncl
;
; Description: Calculates the global average signal to noise ratio for model variables 
; following the Pinatubo eruption using the std of the unforced ensemble as the noise 
; and the difference between ensemble means as the signal. Plots the signal over time.
;
; Date: 9/11/2012
; Created by Abigail Gaddis, University of Tennessee
;========================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; Read in list of files with anomalies already removed.
;----------------------------------------------------------------------------------------

   ;read in FAMIP ensemble decadal anomalies
   dir = "/tmp/work/aag/repos/ncl_analysis/FAMIP/"
   fil = systemfunc("cd "+dir+"  ; ls decAnom*cam*")
   filelist = addfiles(dir+fil, "r")
   vNames = getfilevarnames (filelist[0]) ; get names of all variables in file
   nNames = dimsizes (vNames)   ; number of variables on the file

   ;read in FAMIP minus Pinatubo ensemble decadal anomalies
   noPindir = "/tmp/work/aag/repos/ncl_analysis/noPin/"
   noPinfil = systemfunc("cd "+noPindir+"  ; ls decAnom*cam*")
   noPinfilelist = addfiles(noPindir+noPinfil, "r")

;----------------------------------------------------------------------------------------
; Read in variables from files
;----------------------------------------------------------------------------------------

; Read in dimensions from file, compute dimension sizes
   time = filelist[0]->time
   lat = filelist[0]->lat
   lon = filelist[0]->lon
   lev = filelist[0]->lev
   nmon = dimsizes(time)
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nlevs = dimsizes(lev)
   nens = dimsizes(fil)
   npts = nmon*nlats*nlons

; Create netcdf file to add signals and stds of each var to later
   ncdffile = "signalSTD"
   system ("rm -f " + ncdffile + ".nc")        ;remove pre-existing file
   ncdf = addfile(ncdffile + ".nc" ,"c")       ;open output netCDF file
   setfileoption(ncdf, "DefineMode", True)
   dim_names = (/"time", "lev"/)
   dim_sizes = (/nmon , nlevs/)
   dimUnlim = (/True , False/)
   filedimdef( ncdf, dim_names, dim_sizes, dimUnlim)
   filevardef(ncdf,"time","double",(/"time"/))
   filevarattdef(ncdf,"time",time)
   filevardef(ncdf,"lev",typeof(lev),getvardims(lev))
   filevarattdef(ncdf,"lev",lev) 
   setfileoption(ncdf, "DefineMode", False)
   ncdf->lev = (/lev/)

;Initializing variables outside the loop
   wgt = NormCosWgtGlobe(lat)
   vars_FAMIP4D = new ((/nens, nmon, nlevs, nlats, nlons/), float)
   vars_FAMIP4D!1 = "time"
   vars_FAMIP4D!2 = "lev"
   vars_FAMIP4D!3 = "lat"
   vars_FAMIP4D!4 = "lon"
   vars_FAMIP4D&time = time
   vars_FAMIP4D&lev = lev
   vars_FAMIP4D = 0.0
   vars_noPin4D = vars_FAMIP4D
   vars_FAMIP3D = vars_FAMIP4D (:,:,0,:,:)
   vars_noPin3D = vars_FAMIP3D
   globavs_FAMIP4D = vars_FAMIP4D(:,:,:,0,0)
   globavs_noPin4D = vars_noPin4D(:,:,:,0,0)
   globavs_FAMIP3D = vars_FAMIP3D(:,:,0,0)
   globavs_noPin3D = vars_noPin3D(:,:,0,0)
   ensembleav_FAMIP4D = globavs_FAMIP4D(0,:,:)
   ensembleav_noPin4D = globavs_noPin4D(0,:,:)
   std_FAMIP4D = globavs_FAMIP4D(0,:,:)
   std_noPin4D = globavs_noPin4D(0,:,:)
   ensembleav_FAMIP3D = globavs_FAMIP3D(0,:)
   ensembleav_noPin3D = globavs_noPin3D(0,:)
   std_FAMIP3D = globavs_FAMIP3D(0,:)
   std_noPin3D = globavs_noPin3D(0,:)

 do n= 5,nNames-1   ;read all variables in the file, variable 4 is the first 3D variable
      varname = vNames(n)
      print ("Calculating predictability signals for variable " + varname)
      print ("Variable is " + n + " out of " + (nNames-1))

      if ((n .ge. 51) .and. (n .le. 62)) then            ;not interested in forcing variables
         print ("Variable " + varname + " is aerosol related. Skipping...")
         continue
      end if

      outputfile = varname+"_signal"
      vardims = getfilevardims(filelist[0],varname) ;gets dim names of nth variable in the first filelist file
      vardimnum = dimsizes(vardims)                     ;finds number of dims in nth variable

      ; Read in anomaly for each file for this variable
      if (vardimnum .eq. 3) then
         do i = 0,nens-1
            vars_FAMIP3D(i,:,:,:) = filelist[i]->$varname$
            vars_noPin3D(i,:,:,:) = noPinfilelist[i]->$varname$
         end do
      else if (vardimnum .eq. 4) then
         do i = 0,nens-1
            vars_FAMIP4D(i,:,:,:,:) = filelist[i]->$varname$
            vars_noPin4D(i,:,:,:,:) = noPinfilelist[i]->$varname$
            ;If 30% or more of the level for each case is missing values, blank it out
            do l = 0,nlevs-1
               if (num(ismissing(vars_FAMIP4D(i,:,l,:,:))) .gt. (.3*npts)) then
                  vars_FAMIP4D(i,:,l,:,:) = vars_FAMIP4D@_FillValue
               else if (num(ismissing(vars_noPin4D(i,:,l,:,:))) .gt. (.3*npts)) then
                  vars_noPin4D(i,:,l,:,:) = vars_noPin4D@_FillValue
               end if
               end if
            end do
         end do
      end if
      end if

   ;----------------------------------------------------------------------------------------
   ; Calculate global weighted averages
   ;----------------------------------------------------------------------------------------

      if (vardimnum .eq. 3) then
         do i = 0,nens-1
            globavs_FAMIP3D(i,:) = wgt_areaave_Wrap(vars_FAMIP3D(i,:,:,:), wgt, 1.0, 0)
            globavs_noPin3D(i,:) = wgt_areaave_Wrap(vars_noPin3D(i,:,:,:), wgt, 1.0, 0)
         end do
      else if (vardimnum .eq. 4) then
         do i = 0,nens-1
            globavs_FAMIP4D(i,:,:) = wgt_areaave_Wrap(vars_FAMIP4D(i,:,:,:,:), wgt, 1.0, 0)
            globavs_noPin4D(i,:,:) = wgt_areaave_Wrap(vars_noPin4D(i,:,:,:,:), wgt, 1.0, 0)
         end do
      end if
      end if

   ;----------------------------------------------------------------------------------------
   ; Calculate ensemble averages, stds, and signal to noise ratio over time
   ;----------------------------------------------------------------------------------------

      if (vardimnum .eq. 3) then
         ;Making time series of the ensemble average anomaly
         ensembleav_FAMIP3D = dim_avg_n_Wrap(globavs_FAMIP3D,0)
         ensembleav_noPin3D = dim_avg_n_Wrap(globavs_noPin3D,0)

         std_FAMIP3D = dim_stddev_n(globavs_FAMIP3D,0)
         std_noPin3D = dim_stddev_n(globavs_noPin3D,0)
         if (any(std_noPin3D .eq. 0)) then  ;if the standard deviation is 0 anywhere, skip
            delete(vardims)
            continue
         else
            ens_diff = ensembleav_FAMIP3D - ensembleav_noPin3D
            signal = globavs_FAMIP3D(0,:)
            signal = abs(ens_diff / std_noPin3D)
         end if
         if ((ismissing(max(signal))) .or. (max(signal) .lt. 3)) then  ;Smax < 3, skip
            print( "Max signal is less than 3. Skipping plots for 3D variable " + varname)
            ; deleting variables that change size if 3D vs 4D
            delete(vardims)
            delete(ens_diff)
            delete(signal)
            continue
         end if
      else if (vardimnum .eq. 4) then

         ;Making time, level series of the ensemble average anomaly
         ensembleav_FAMIP4D = dim_avg_n_Wrap(globavs_FAMIP4D,0)
         ensembleav_noPin4D = dim_avg_n_Wrap(globavs_noPin4D,0)

         std_FAMIP4D = dim_stddev_n(globavs_FAMIP4D,0)
         std_noPin4D = dim_stddev_n(globavs_noPin4D,0)
         ens_diff = ensembleav_FAMIP4D - ensembleav_noPin4D
         signal = ensembleav_FAMIP4D(lev|:,time|:)
         do l = 0,nlevs-1
            if (any (std_noPin4D(:,l) .eq. 0)) then
               signal(l,:) = signal@_FillValue
               std_noPin4D(:,l) = std_noPin4D@_FillValue
               std_FAMIP4D(:,l) = std_FAMIP4D@_FillValue
            else
               signal(l,:) = abs(ens_diff(:,l) / std_noPin4D(:,l))
            end if
         end do
         if ((ismissing(max(signal))) .or. (max(signal) .lt. 3)) then  ;Smax < 3, skip
            print( "Max signal for all levels is less than 3. Skipping plots for variable " + varname)
            ; deleting variables that change size if 3D vs 4D
            delete(vardims)
            delete(ens_diff)
            delete(signal)
            continue
         end if
      end if
      end if

   ;----------------------------------------------------------------------------------------
   ; Use output to create plots, add to netcdf file
   ;----------------------------------------------------------------------------------------

   ; Netcdf file output settings
      if (n.eq.4) then
         ncdf->time = (/time/)
      end if
   ;Adding signal(t) and std(t) to output file
      var1 = varname + "_signal" 
      var2 = varname + "_std_noPin"
      var3 = varname + "_std_FAMIP" 
      if (vardimnum .eq. 4) then
         ncdf->$var1$ = signal(time|:,lev|:)
         ncdf->$var2$ = std_noPin4D
         ncdf->$var3$ = std_FAMIP4D
      else
         ncdf->$var1$ = signal
         ncdf->$var2$ = std_noPin3D
         ncdf->$var3$ = std_FAMIP3D
      end if

   ; Signal over time plot settings
      wks                          = gsn_open_wks("pdf",outputfile)
      res                          = True

      coloropt = True
      coloropt@NumColorsInTable = 20
      colors = (/"white","cyan","navyblue", "black"/)
      bluefade = span_named_colors(colors,coloropt)
      gsn_define_colormap(wks,bluefade)

;      setvalues wks            
;         "wkColorMap"        : "gsltod"  
;         "wkForegroundColor" : (/0.,0.,0./)  
;         "wkBackgroundColor" : (/1.,1.,1./) 
;      end setvalues

      labels = "CESM1.0"
      res@tiXAxisString            = "Time (years)"
      res@tiMainFontHeightF        = 0.020
      res@tiXAxisFontHeightF       = 0.020
      res@tiYAxisFontHeightF       = 0.020
      res@gsnYRefLine              = 1.0
      res@tmLabelAutoStride        = True
      res@tmXTOn                 = False
      res@tmYROn                 = False
      res@tmXBMode                 = "Explicit"
      res@tmXBValues               = (/time(0),time(12),time(24),time(36)/)
      res@tmXBLabels               = (/"1991","1992","1993","1994"/)
      res@tmXBMinorValues          = time
      if (vardimnum .eq. 3) then
         res@tiYAxisString = vars_FAMIP3D@long_name+ " signal"
         plot = gsn_csm_xy(wks,time,signal,res)
      else
         ; Signal for 4D variables prints as a contour plot, settings below
         rescn                        = True
         rescn@cnFillOn               = True
         rescn@tiMainString           = vars_FAMIP4D@long_name+ " signal"
         rescn@tiYAxisString          = "Vertical pressure level (mb)"
         rescn@tiXAxisString          = "Time (years)"
         rescn@tiMainFontHeightF      = 0.020
         rescn@tiXAxisFontHeightF     = 0.020
         rescn@tiYAxisFontHeightF     = 0.020
         rescn@tmYLMode               = "Explicit"
         rescn@tmYLValues             = (/5,20,50,100,200,500,990/)
         rescn@tmYLLabels             = (/"5","20","50","100","200","500","990"/)
         rescn@tmYLMinorValues        = lev
         rescn@tmYROn                 = False
         rescn@tmXBMode               = "Explicit"
         rescn@tmXBValues               = (/time(0),time(12),time(24),time(36)/)
         rescn@tmXBLabels               = (/"1991","1992","1993","1994"/)
         rescn@tmXBMinorValues        = time
         rescn@tmXTOn                 = False
         rescn@gsnSpreadColors        = True
         rescn@cnLinesOn              = False        ; True is default
         rescn@cnLineLabelsOn         = False        ; True is default
         rescn@lbLabelAutoStride      = True         ; auto stride on labels
         rescn@gsnLeftString          = " "
         rescn@gsnRightString         = " "
         rescn@trYReverse             = True 
         rescn@gsnYAxisIrregular2Log  = True
         ;rescn@cnLevelSelectionMode    =  "ManualLevels"
         ;rescn@cnMinLevelValF          = 0
         ;rescn@cnMaxLevelValF          = max(signal)
         ;rescn@cnLevelSpacingF         = max(signal)/30

         plot = gsn_csm_contour(wks,signal(:,:),rescn)               
      end if

   ; deleting plotting resources
      delete (plot)
      delete (wks)
   ; deleting variables that change size if 3D vs 4D
      delete(vardims)
      delete(ens_diff)
      delete(signal)
   end do

