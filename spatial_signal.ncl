;========================================================================================
; Filename: regional_signal.ncl
; Description: Analyzes predictability signals of variables from two ensembles of model 
; runs, calculating a signal to noise ratio for the predictability. This is done for all
; grid points in the model, and an alternate signal using EOFs is calculated.
; Date: 8/13/2012
; Created by Abigail Gaddis, University of Tennessee
;========================================================================================

   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
   load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;----------------------------------------------------------------------------------------
; User settings
;----------------------------------------------------------------------------------------

   ;Select time periods for analysis

   ;File labeling and names
   dataset1     = "CESM1.0 FAMIP ensemble"
   dataset2     = "CESM1.0 FAMIP no Pinatubo ensemble"

   neval = 3   ;number of eofs to evaluate

;----------------------------------------------------------------------------------------
; Read in list of files
;----------------------------------------------------------------------------------------

   ;read in FAMIP ensemble directory for climatology
   dir = "/tmp/work/aag/repos/ncl_analysis/FAMIP/"
   fil = systemfunc("cd "+dir+"  ; ls decAnom*cam*")
;   fil = systemfunc("cd "+dir+"  ; ls *clm*")
   filelist = addfiles(dir+fil, "r")
   vNames = getfilevarnames (filelist[0]) ; get names of all variables in file
   nNames = dimsizes (vNames)   ; number of variables on the file

   ;read in FAMIP minus Pinatubo ensemble directory for anomaly
   noPindir = "/tmp/work/aag/repos/ncl_analysis/noPin/"
   noPinfil = systemfunc("cd "+noPindir+"  ; ls decAnom*cam*")
;   noPinfil = systemfunc("cd "+noPindir+"  ; ls *clm*")
   noPinfilelist = addfiles(noPindir+noPinfil, "r")

;----------------------------------------------------------------------------------------
; Read in variables over selected time periods
;----------------------------------------------------------------------------------------

   ;selecting variables over desired time ranges for average and anomaly
   lat = filelist[0]->lat
   lon = filelist[0]->lon
   lev = filelist[0]->lev
   time = filelist[0]->time
   ens = fil
   nlats = dimsizes(lat)
   nlons = dimsizes(lon)
   nlevs = dimsizes(lev)
   nmon = dimsizes(time)
   nens = dimsizes(fil)
   ;warning is for line below, can't fix due to the way the file is (var "lat" has dimension 0 = "lat")
   wgt    = NormCosWgtGlobe(lat)
   nvars = 10
   npts = nlats*nlons

; Initializing variables
   var_FAMIP3D = new ((/nens, nmon, nlevs, nlats, nlons/), float)
   var_FAMIP3D!0 = "ens"
   var_FAMIP3D!1 = "time"
   var_FAMIP3D!2 = "lev"
   var_FAMIP3D!3 = "lat"
   var_FAMIP3D!4 = "lon"
   var_FAMIP3D&ens = ens
   var_FAMIP3D&time = time
   var_FAMIP3D&lat = lat
   var_FAMIP3D&lon = lon
   var_FAMIP3D&lev = lev

   var_noPin3D = var_FAMIP3D
   var_FAMIP2D = var_FAMIP3D(:,:,0,:,:)
   var_noPin2D = var_FAMIP2D

   evec_FAMIP3D = new ((/nens, nlevs, neval, nlats, nlons/), double)
   evec_FAMIP3D!3 = "lat"
   evec_FAMIP3D!4 = "lon"
   evec_FAMIP3D&lat = lat
   evec_FAMIP3D&lon = lon
   evec_noPin3D = evec_FAMIP3D
   evects_FAMIP3D = new ((/nens,nlevs,neval,nmon/), double)
   evects_noPin3D = evects_FAMIP3D

   evec_FAMIP2D = evec_FAMIP3D(:,0,:,:,:)
   evec_noPin2D = evec_FAMIP2D 
   evects_FAMIP2D = evects_FAMIP3D(:,0,:,:)
   evects_noPin2D = evects_FAMIP2D 

   std_FAMIP3D = var_FAMIP3D(0,:,:,:,:)
   std_noPin3D = var_noPin3D(0,:,:,:,:)
   std_FAMIP2D = var_FAMIP2D(0,:,:,:)
   std_noPin2D = var_noPin2D(0,:,:,:)

   signal3D = std_FAMIP3D
   signal2D = std_FAMIP2D

   eofdiff3D = evec_FAMIP3D(0,:,:,:,:)
   eofdiff2D = eofdiff3D(0,:,:,:)

;   winter_FAMIP3D = new ((/nens,3,nlevs,nlats,nlons/), float)
;   winter_FAMIP3D!0 = "ens"
;   winter_FAMIP3D!3 = "lat"
;   winter_FAMIP3D!4 = "lon"
;   winter_FAMIP3D&ens = ens
;   winter_FAMIP3D&lat = lat
;   winter_FAMIP3D&lon = lon
;   winter_noPin3D = winter_FAMIP3D
;   summer_FAMIP3D = winter_FAMIP3D
;   summer_noPin3D = winter_FAMIP3D
;   winter_FAMIP2D = winter_FAMIP3D(:,:,0,:,:)
;   winter_noPin2D = winter_FAMIP2D
;   summer_FAMIP2D = winter_FAMIP2D
;   summer_noPin2D = winter_FAMIP2D

; Read all variables in the ensemble files, variable 38 is the first 3D variable
   do n = 13,nNames-1  ;4,nNames-1
      varname = vNames(n)
      print ("Mapping spatial predictability signals for " + varname)
      print ("Variable is " + n + " out of " + (nNames-1))

      outputfile = varname+"_spatial"

      vardims = getfilevardims(filelist[0],varname) ;gets dim names of nth variable in the first filelist file
      vardimnum = dimsizes(vardims)                     ;finds number of dims in nth variable

      ; Read in anomaly for each file for this variable
      if (vardimnum .eq. 3) then
         do i = 0,nens-1
            var_FAMIP2D(i,:,:,:) = filelist[i]->$varname$
            var_noPin2D(i,:,:,:) = noPinfilelist[i]->$varname$
         end do
      else if (vardimnum .eq. 4) then
         do i = 0,nens-1
            var_FAMIP3D(i,:,:,:,:) = filelist[i]->$varname$
            var_noPin3D(i,:,:,:,:) = noPinfilelist[i]->$varname$
         end do
      end if
      end if

   ;----------------------------------------------------------------------------------------
   ; Calculate EOFs and the time series of the eigenvalues
   ;----------------------------------------------------------------------------------------

      if (vardimnum .eq. 3) then
         wgtVar_FAMIP2D = var_FAMIP2D*conform(var_FAMIP2D, wgt, 2)
         wgtVar_noPin2D = var_noPin2D*conform(var_noPin2D, wgt, 2)
         copy_VarMeta(var_FAMIP2D,wgtVar_FAMIP2D)
         copy_VarMeta(var_noPin2D,wgtVar_noPin2D)

         do i=0,nens-1
            evec_FAMIP2D(i,:,:,:) = eofunc_Wrap(wgtVar_FAMIP2D(ens|i,lat|:,lon|:,time|:),neval,False)
            evects_FAMIP2D(i,:,:) = eofunc_ts_Wrap(wgtVar_FAMIP2D(ens|i,lat|:,lon|:,time|:),evec_FAMIP2D(i,:,:,:),False)
            evec_noPin2D(i,:,:,:) = eofunc_Wrap(wgtVar_noPin2D(ens|i,lat|:,lon|:,time|:),neval,False)
            evects_noPin2D(i,:,:) = eofunc_ts_Wrap(wgtVar_noPin2D(ens|i,lat|:,lon|:,time|:),evec_noPin2D(i,:,:,:),False)
         end do

      else if (vardimnum .eq. 4) then
         wgtVar_FAMIP3D = var_FAMIP3D*conform(var_FAMIP3D, wgt, 3)
         wgtVar_noPin3D = var_noPin3D*conform(var_noPin3D, wgt, 3)
         copy_VarMeta(var_FAMIP3D,wgtVar_FAMIP3D)
         copy_VarMeta(var_noPin3D,wgtVar_noPin3D)

         ; do first 3 eofs of each level
         do i = 0,nens-1
            do l = 0,nlevs-1
               ;if many of the numbers are missing or zero, skip the level
               if ((num(wgtVar_FAMIP3D(i,:,l,:,:) .eq. 0) .gt. (.4*npts*nmon)) .or. (num(ismissing(wgtVar_FAMIP3D(i,:,l,:,:))) .gt. (.3*npts*nmon))) then
;                  print ("More than 40% of the values are 0 or more than 30% of the values are missing. Skipping level.")
                  wgtVar_FAMIP3D(i,:,l,:,:) = var_FAMIP3D@_FillValue
                  continue
               else
                  evec_FAMIP3D(i,l,:,:,:) = eofunc_Wrap(wgtVar_FAMIP3D(ens|i,lev|l,lat|:,lon|:,time|:),neval,False) 
                  evects_FAMIP3D(i,l,:,:) = eofunc_ts_Wrap(wgtVar_FAMIP3D(ens|i,lev|l,lat|:,lon|:,time|:),evec_FAMIP3D(i,l,:,:,:),False)
                  evec_noPin3D(i,l,:,:,:) = eofunc_Wrap(wgtVar_noPin3D(ens|i,lev|l,lat|:,lon|:,time|:),neval,False)
                  evects_noPin3D(i,l,:,:) = eofunc_ts_Wrap(wgtVar_noPin3D(ens|i,lev|l,lat|:,lon|:,time|:),evec_noPin3D(i,l,:,:,:),False)
               end if
            end do
         end do
      end if
      end if

   ;----------------------------------------------------------------------------------------
   ; Calculate ensemble averages, stds, and signal to noise ratio over time
   ;----------------------------------------------------------------------------------------

      if (vardimnum .eq. 3) then

         ;Calculating ensemble averages, eofs, and seasonal averages
         
         ensembleav_FAMIP2D = dim_avg_n_Wrap(var_FAMIP2D, 0)
         ensembleav_noPin2D = dim_avg_n_Wrap(var_noPin2D, 0)
         eof_ensav_FAMIP2D = dim_avg_n_Wrap(evec_FAMIP2D,0)
         eof_ensav_noPin2D = dim_avg_n_Wrap(evec_noPin2D,0)
         eofts_ensav_FAMIP2D = dim_avg_n_Wrap(evects_FAMIP2D,0)
         eofts_ensav_noPin2D = dim_avg_n_Wrap(evects_noPin2D,0)

         std_FAMIP2D = dim_stddev_n(var_FAMIP2D,0)
         std_noPin2D = dim_stddev_n(var_noPin2D,0)

         ;In the locations where the std is zero, use a fill value and for the std and set the signal to zero
         std_noPin2D = where(std_noPin2D .eq. 0, std_noPin2D@_FillValue, std_noPin2D) 
         ens_diff2D = ensembleav_FAMIP2D - ensembleav_noPin2D
         eofdiff2D = eof_ensav_FAMIP2D - eof_ensav_noPin2D
         signal2D = ens_diff2D / std_noPin2D
         signalAv2D = dim_avg_n_Wrap(signal2D,0)
         signalWinter2D = month_to_season(signal2D(12:47,:,:), "DJF")
         signalSummer2D = month_to_season(signal2D(0:35,:,:), "JJA")
         winterAvDiff2D = dim_avg_n_Wrap(signalWinter2D,0)
         summerAvDiff2D = dim_avg_n_Wrap(signalSummer2D,0)

      else if (vardimnum .eq. 4) then
         ;Calculating ensemble averages, eofs, and seasonal averages

         ensembleav_FAMIP3D = dim_avg_n_Wrap(var_FAMIP3D, 0)
         ensembleav_noPin3D = dim_avg_n_Wrap(var_noPin3D, 0)

         eof_ensav_FAMIP3D = dim_avg_n_Wrap(evec_FAMIP3D,0)
         eof_ensav_noPin3D = dim_avg_n_Wrap(evec_noPin3D,0)
         eofts_ensav_FAMIP3D = dim_avg_n_Wrap(evects_FAMIP3D,0)
         eofts_ensav_noPin3D = dim_avg_n_Wrap(evects_noPin3D,0)

         std_FAMIP3D = dim_stddev_n(var_FAMIP3D,0)
         std_noPin3D = dim_stddev_n(var_noPin3D,0)

         signalAv3D = dim_avg_n_Wrap(signal3D,0)
         signalWinter3D = month_to_season(signal3D(12:47,:,:,:), "DJF")
         signalSummer3D = month_to_season(signal3D(0:35,:,:,:), "JJA")
         winterAvDiff3D = dim_avg_n_Wrap(signalWinter3D,0)
         summerAvDiff3D = dim_avg_n_Wrap(signalSummer3D,0)

         ;In the locations where the std is zero, use a fill value and for the std and set the signal to zero
         std_noPin3D = where(std_noPin3D .eq. 0, std_noPin3D@_FillValue, std_noPin3D)
         signal3D = ((ensembleav_FAMIP3D - ensembleav_noPin3D) / std_noPin3D)
         signalAv3D = dim_avg_n_Wrap(signal3D,0)
         eofdiff3D = eof_ensav_FAMIP3D - eof_ensav_noPin3D

      end if
      end if

   ;----------------------------------------------------------------------------------------
   ; Use output to create plots
   ;----------------------------------------------------------------------------------------

   ; General plotting settings
      wks          = gsn_open_wks("pdf",outputfile)
      plot = new(3, graphic)
      temp1 = new(3,graphic)
      temp2 = new(neval*2,graphic)
      temp3 = new(neval*2,graphic)
      plot2 = new(neval,graphic)
      plot3 = plot2
      plot4 = plot2
      plot5 = plot2
      plot6 = plot2

      gsn_define_colormap(wks,"BlueWhiteOrangeRed")
;      gsn_define_colormap(wks,"BlGrYeOrReVi200")
      months = ispan(0,nmon-1,1)

      rescn = True
      rescn@cnFillOn                 = True
      rescn@gsnSpreadColors          = True
      rescn@gsnAddCyclic             = True
      rescn@cnLinesOn                = False        ; True is default
      rescn@cnLineLabelsOn           = False        ; True is default
      rescn@lbLabelAutoStride        = True         ; auto stride on labels
      rescn@tmYROn = False
      rescn@tmXTOn = False

      resP                           = True         ; panel resources
      resP@gsnMaximize               = True

      rts = True
;      rts@tiXAxisString            = "Time (years)"
      rts@tiYAxisString            = ""
      rts@trXMaxF                   = time(47)
      rts@tmXBMode                 = "Explicit"
      rts@tmXBValues               = (/time(0),time(12),time(24),time(36)/)
      rts@tmXBLabels               = (/"1991","1992","1993","1994"/)
      rts@tmXBMinorValues          = time ;months
      rts@tmYROn = False
      rts@tmXTOn = False

     ; Lat/lon variables
      if (vardimnum .eq. 3) then
         resP@txString             = var_FAMIP2D@long_name+ " signal"
        ; Spatial maps of signal at certain times
         rescn@gsnDraw   = False       ; don't draw yet
         rescn@gsnFrame  = False       ; don't advance frame yet
         rescn@gsnScale  = True        ; force text scaling
         rescn@gsnLeftString = ""
         rescn@gsnRightString = var_FAMIP2D@long_name

         if (all(ismissing(signal2D)) .or. (max(signal2D) .le. 6))  then
            print("Variable contains only missing values or signal max is less than 6. Skipping.")
           ; deleting plotting resources
            delete (plot)
            delete (plot2)
            delete (wks)
            delete (rescn)
           ; deleting variables that change size if 3D vs 4D
            delete(vardims)
            continue
         else
            rescn@gsnLeftString = "Average signal"
            plot(0) = gsn_csm_contour_map(wks,signalAv2D,rescn)
            rescn@gsnLeftString = "Winter average signal"
            plot(1) = gsn_csm_contour_map(wks,winterAvDiff2D,rescn)
            rescn@gsnLeftString = "Summer average signal"
            plot(2) = gsn_csm_contour_map(wks,summerAvDiff2D,rescn)
            gsn_panel(wks,plot,(/3,1/),resP)

         end if

      ; EOF coefficient time series plot
         rts@gsnDraw   = False       ; don't draw yet
         rts@gsnFrame  = False       ; don't advance frame yet
         rts@gsnScale  = True        ; force text scaling
         rescn@gsnDraw   = False       ; don't draw yet
         rescn@gsnFrame  = False       ; don't advance frame yet
         rescn@gsnRightString = ""
         do k=0,neval-1
            rts@gsnLeftString  = "EOF coefficient "+(k+1)
            rescn@gsnLeftString = "EOF "+(k+1)
            plot2(k) = gsn_csm_contour_map(wks,eofdiff2D(k,:,:),rescn)
            rescn@gsnLeftString = "EOF "+(k+1)+ " forced"
            rts@gsnLeftString  = "EOF "+(k+1)+ " forced"
            plot3(k) = gsn_csm_contour_map(wks,eof_ensav_FAMIP2D(k,:,:),rescn)
            plot4(k) = gsn_csm_xy(wks,time,eofts_ensav_FAMIP2D(k,:),rts)
            rescn@gsnLeftString = "EOF "+(k+1)+ " unforced"
            rts@gsnLeftString  = "EOF "+(k+1)+ " unforced"
            plot5(k) = gsn_csm_contour_map(wks,eof_ensav_noPin2D(k,:,:),rescn)
            plot6(k) = gsn_csm_xy(wks,time,eofts_ensav_noPin2D(k,:),rts)
         end do
         do p =0,neval-1
           temp2(2*p) = plot3(p)
           temp2(2*p+1)=plot5(p)
           temp3(2*p) = plot4(p)
           temp3(2*p+1) = plot6(p) 
         end do

         resP@txString = "Difference of forced and unforced EOFs ~C~" + var_FAMIP2D@long_name
         gsn_panel(wks,plot2,(/neval,1/),resP)
         resP@txString = "Ensemble average EOF ~C~ " + var_FAMIP2D@long_name
         gsn_panel(wks,temp2,(/neval,2/),resP)
         resP@txString = "Ensemble average EOF time series ~C~ " + var_FAMIP2D@long_name
         gsn_panel(wks,temp3,(/neval,2/),resP)

   ; Lat/lon/level variables
      else if (vardimnum .eq. 4) then
         do l = 0, nlevs-1, 3
            if ((num(ismissing(signal3D(:,l,:,:))) .gt. (.3*npts)) .or. (min(signal3D(:,l,:,:)) .eq. max(signal3D(:,l,:,:))))  ;skip if mostly missing data
               print("Skipping level. Level has greater than 30% missing values.")
               continue
            else
              ; Spatial maps of signal at certain times
               rescn@gsnDraw   = False       ; don't draw yet
               rescn@gsnFrame  = False       ; don't advance frame yet
               rescn@gsnScale  = True        ; force text scaling
               rescn@gsnRightString = "Vertical level sigma = " + sprintf("%1.2f",lev(l))
               rescn@gsnLeftString = "Average signal"
               plot(0) = gsn_csm_contour_map(wks,signalAv3D(l,:,:),rescn)
               rescn@gsnLeftString = "Winter average signal"
               plot(1) = gsn_csm_contour_map(wks,winterAvDiff3D(l,:,:),rescn)
               rescn@gsnLeftString = "Summer average signal"
               plot(2) = gsn_csm_contour_map(wks,summerAvDiff3D(l,:,:),rescn)
               resP@txString         = var_FAMIP3D@long_name
               gsn_panel(wks,plot,(/3,1/),resP)

              ; EOF coefficient time series plot
               rts@gsnDraw   = False       ; don't draw yet
               rts@gsnFrame  = False       ; don't advance frame yet
               rts@gsnScale  = True        ; force text scaling
               do k=0,neval-1
                  rts@gsnRightString = "Vertical level sigma = " + sprintf("%1.2f",lev(l))
                  rescn@gsnLeftString = "EOF "+(k+1)
                  rescn@gsnRightString = "Vertical level sigma = " + sprintf("%1.2f",lev(l))
                  plot2(k) = gsn_csm_contour_map(wks,eofdiff3D(l,k,:,:),rescn)
                  rescn@gsnLeftString = "EOF "+(k+1)+ " forced"
                  rts@gsnLeftString  = "EOF "+(k+1)+ " forced"
                  plot3(k) = gsn_csm_contour_map(wks,eof_ensav_FAMIP3D(l,k,:,:),rescn)
                  plot4(k) = gsn_csm_xy(wks,time,eofts_ensav_FAMIP3D(l,k,:),rts)
                  rescn@gsnLeftString = "EOF "+(k+1)+ " unforced"
                  rts@gsnLeftString  = "EOF "+(k+1)+ " unforced"
                  plot5(k) = gsn_csm_contour_map(wks,eof_ensav_noPin3D(l,k,:,:),rescn)
                  plot6(k) = gsn_csm_xy(wks,time,eofts_ensav_noPin3D(l,k,:),rts)
               end do
               do p =0,2
                  temp2(2*p)  =plot3(p)
                  temp2(2*p+1)=plot5(p)
                  temp3(2*p)  =plot4(p)
                  temp3(2*p+1)=plot6(p)
               end do
               resP@txString = "Difference of forced and unforced EOFs ~C~" + var_FAMIP3D@long_name
               gsn_panel(wks,plot2,(/neval,1/),resP)
               resP@txString = "Ensemble average EOF ~C~ " + var_FAMIP3D@long_name
               gsn_panel(wks,temp2,(/neval,2/),resP)
               resP@txString = "Ensemble average EOF coefficient ~C~ " + var_FAMIP3D@long_name
               gsn_panel(wks,temp3,(/neval,2/),resP)
               end if
         end do
      end if
      end if

   ; deleting plotting resources
      delete (plot)
      delete (temp1)
      delete (temp2)
      delete (plot2)
      delete (wks)
      delete (rts)
      delete (rescn)
   ; deleting variables that change size if 3D vs 4D
      delete(vardims)
   end do

